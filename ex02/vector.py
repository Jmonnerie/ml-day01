# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    vector.py                                          :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/27 00:36:01 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/27 00:36:01 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import math


def can_float_cast(s):
    try:
        num = float(s)
    except TypeError:
        return False
    except ValueError:
        return False
    if math.isnan(num) or math.isinf(num):
        return False
    return True


class Vector():
    def __init__(self, data):
        self.values = []
        usage = "__init__() takes an int, a list of numbers or " +\
                "a tuple of 2 numbers"
        if type(data) == int:
            self.values = [round(float(n), 1) for n in range(0, data)]
            self.size = data
        elif type(data) == list:
            for elem in data:
                if not can_float_cast(elem):
                    raise TypeError(
                        usage +
                        f"\nAnd \"{elem}\" in {data} is not a number")
            self.values = [round(float(n), 1) for n in data]
            self.size = len(data)
        elif type(data) == tuple:
            if len(data) != 2:
                raise TypeError(
                    usage +
                    "\nAnd the tuple lenght is not 2")
            if not (can_float_cast(data[0]) and can_float_cast(data[1])):
                raise TypeError(
                    usage +
                    "\nAnd values in the tuple are not numbers")
            self.values = [round(float(n), 1) for n in range(data[0], data[1])]
            self.size = data[1] - data[0]
        else:
            raise TypeError(usage)

    def __add__(self, other):
        """ Addition between 2 vectors """
        if isinstance(other, Vector):
            if self.size != other.size:
                raise ValueError("Can only add 2 vectors of the same size " +
                                 f"({self.size} != {other.size})")
            return Vector([self.values[i] + other.values[i]
                           for i in range(self.size)])
        return NotImplemented

    def __radd__(self, other):
        """ Addition between 2 vectors """
        if isinstance(other, Vector):  # anti infinite loop
            return self + other
        return NotImplemented

    def __sub__(self, other):
        """ Substraction between 2 vectors """
        if isinstance(other, Vector):
            if self.size != other.size:
                raise ValueError("Can only sub 2 vectors of the same size " +
                                 f"({self.size} != {other.size})")
            return Vector([self.values[i] - other.values[i]
                           for i in range(self.size)])
        return NotImplemented

    def __rsub__(self, other):
        """ Substraction between 2 vectors """
        if isinstance(other, Vector):
            if self.size != other.size:
                raise ValueError("Can only sub 2 vectors of the same size " +
                                 f"({self.size} != {other.size})")
            return Vector([other.values[i] - self.values[i]
                           for i in range(self.size)])
        return NotImplemented

    def __truediv__(self, other):
        """ Division between a vector and a number """
        if can_float_cast(other):
            if float(other) == 0:
                raise ZeroDivisionError("can not divide vector by zero")
            return Vector([round(n / float(other), 1)
                          for n in self.values])
        return NotImplemented

    def __rtruediv__(self, other):
        """ Division by a vector isn't possible"""
        return NotImplemented

    def __mul__(self, other):
        """ Multiplication between a vector and a number or another vector"""
        if can_float_cast(other):
            return Vector([round(n * float(other), 1)
                          for n in self.values])
        if isinstance(other, Vector):
            if self.size != other.size:
                raise ValueError("Can only mult 2 vectors of the same size " +
                                 f"({self.size} != {other.size})")
            ret = 0
            for i in range(self.size):
                ret += self.values[i] * other.values[i]
            return ret
        return NotImplemented

    def __rmul__(self, other):
        """ Multiplication between a vector and a number or another vector"""
        if isinstance(other, Vector) or can_float_cast(other):
            return self * other
        return NotImplemented

    def __str__(self):
        return f"{self.__class__}(\n" +\
               f"\tvalues = {self.values}\n" +\
               f"\tsize   = {self.size}\n" +\
               ")"

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)
