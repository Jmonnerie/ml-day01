# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    test.py                                            :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/27 00:36:35 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/27 00:36:35 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#


import unittest
import sys
from vector import Vector


class TestVectorInit(unittest.TestCase):
    # Initialisation tests

    def test_vector_init_from_int(self):
        self.vI = Vector(5)
        self.assertListEqual(self.vI.values, [0.0, 1.0, 2.0, 3.0, 4.0])
        self.assertEqual(self.vI.size, 5)

    def test_vector_init_from_range(self):
        self.vR = Vector((10, 15))
        self.assertListEqual(self.vR.values, [10, 11, 12, 13, 14])
        self.assertEqual(self.vR.size, 5)

    def test_vector_init_from_list(self):
        self.vL = Vector([0.3, 0.5, 0.8])
        self.assertListEqual(self.vL.values, [0.3, 0.5, 0.8])
        self.assertEqual(self.vL.size, 3)


class TestVectorMagics(unittest.TestCase):
    # Magic function tests

    def setUp(self):
        self.v1 = Vector((0, 5))
        self.v2 = Vector((0, 5))
        self.vDifSize = Vector((0, 6))

    def test_add(self):
        v3 = self.v1 + self.v2
        self.assertListEqual(v3.values, [0.0, 2.0, 4.0, 6.0, 8.0])
        self.assertEqual(v3.size, 5)
        # Errors
        with self.assertRaises(TypeError):
            self.v1 + 5
        with self.assertRaises(TypeError):
            self.v1 + ["array"]
        with self.assertRaises(ValueError):
            self.vDifSize + self.v1

    def test_radd(self):
        v3 = self.v1.__radd__(self.v2)
        self.assertListEqual(v3.values, [0.0, 2.0, 4.0, 6.0, 8.0])
        self.assertEqual(v3.size, 5)
        # Errors
        self.assertEqual(self.v1.__radd__(5), NotImplemented)
        self.assertEqual(self.v1.__radd__(["array"]), NotImplemented)
        with self.assertRaises(ValueError):
            self.vDifSize.__radd__(self.v1)

    def test_sub(self):
        v3 = self.v1 - self.v2
        self.assertListEqual(v3.values, [0.0, 0.0, 0.0, 0.0, 0.0])
        self.assertEqual(v3.size, 5)
        # Errors
        with self.assertRaises(TypeError):
            self.v1 - 5
        with self.assertRaises(TypeError):
            self.v1 + ["array"]
        with self.assertRaises(ValueError):
            self.vDifSize - self.v1

    def test_rsub(self):
        v3 = self.v1.__rsub__(self.v2)
        self.assertListEqual(v3.values, [0.0, 0.0, 0.0, 0.0, 0.0])
        self.assertEqual(v3.size, 5)
        # Errors
        self.assertEqual(self.v1.__rsub__(5), NotImplemented)
        self.assertEqual(self.v1.__rsub__(["array"]), NotImplemented)
        with self.assertRaises(ValueError):
            self.vDifSize.__rsub__(self.v1)

    def test_truediv(self):
        v3 = self.v1 / 2
        self.assertListEqual(v3.values, [0.0, 0.5, 1.0, 1.5, 2.0])
        self.assertEqual(v3.size, 5)
        # Errors
        with self.assertRaises(ZeroDivisionError):
            self.v1 / 0
        with self.assertRaises(TypeError):
            self.v1 / self.v2
        with self.assertRaises(TypeError):
            self.v1 / ["array"]

    def test_rtruediv(self):
        with self.assertRaises(TypeError):
            5 / self.v1

    def test_mul(self):
        v3 = self.v1 * self.v2
        self.assertEqual(v3, 30)
        v3 = self.v1 * 2.5
        self.assertListEqual(v3.values, [0.0, 2.5, 5.0, 7.5, 10.0])
        self.assertEqual(v3.size, 5)
        # Errors
        with self.assertRaises(TypeError):
            self.v1 * "string"
        with self.assertRaises(TypeError):
            self.v1 * ["array"]
        with self.assertRaises(ValueError):
            self.vDifSize * self.v1

    def test_rmul(self):
        v3 = self.v1.__rmul__(self.v2)
        self.assertEqual(v3, 30)
        v3 = 2.5 * self.v1
        self.assertListEqual(v3.values, [0.0, 2.5, 5.0, 7.5, 10.0])
        self.assertEqual(v3.size, 5)
        # Errors
        self.assertEqual(self.v1.__rmul__("string"), NotImplemented)
        self.assertEqual(self.v1.__rmul__(["array"]), NotImplemented)
        with self.assertRaises(ValueError):
            self.vDifSize.__rmul__(self.v1)


if __name__ == "__main__":
    unittest.main()
