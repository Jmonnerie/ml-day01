# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    recipe.py                                          :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/26 12:33:44 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/26 12:33:44 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#


class Recipe():
    """
        A representation of a recipe.

        :param name: The recipe's name.
        :param cooking_lvl: The recipe's difficulty, from 1 to 5.
        :param cooking_time: The recipe's time to cook in minutes, no negative.
        :param ingredients: List of all ingredients each represented by a
                            string.
        :param description: The recipe's descripton.
        :param recipe_type: Can be "starter", "lunch" or "dessert".
        :raises
    """
    name: str
    cooking_lvl: int
    ingredients: list
    description: str
    recipe_type: str

    def __init__(self, name, cooking_lvl, cooking_time,
                 ingredients, description, recipe_type):
        assert name,\
               "BadParamRecipe: name couldn't be empty"
        assert isinstance(name, str),\
               "BadParamRecipe: name should be a string"
        assert isinstance(cooking_lvl, int) and\
               cooking_lvl > 0 and\
               cooking_lvl < 6,\
               "BadParamRecipe: cooking_lvl have to be an int between 1 and 5"
        assert isinstance(cooking_time, int) and cooking_time > 0,\
               "BadParamRecipe: cooking_time have to be a positive int"
        assert isinstance(ingredients, list) and len(ingredients) != 0,\
               "BadParamRecipe: ingredients have to be list wich isn't empty"
        assert recipe_type in ["starter", "lunch", "dessert"],\
               "BadParamRecipe: type have to be starter, lunch or dessert"

        self.name = name
        self.cooking_lvl = cooking_lvl
        self.cooking_time = cooking_time
        self.ingredients = ingredients
        self.description = description
        self.recipe_type = recipe_type
        pass

    def __str__(self):
        return f"{self.name.capitalize()}'s recipe:\n"\
             + f"\tDescription:           {self.description}\n"\
             + f"\tType:                  {self.recipe_type}\n"\
             + f"\tDifficulty:            {self.cooking_lvl}\n"\
             + f"\tAverage cooking time:  {self.cooking_time} minutes\n"\
             + f"\tingredients:           {self.ingredients}\n"
