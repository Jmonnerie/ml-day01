# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    test.py                                            :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/26 12:32:28 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/26 12:33:21 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import unittest

from book import Book, AlreadyExistsError, NotFound
from recipe import Recipe


class TestRecipe(unittest.TestCase):
    def setUp(self):
        self.baseRecipe = {
           "name": "Awesome recipe",
           "cooking_lvl": 5,
           "cooking_time": 10000,
           "ingredients": ["love", "plants"],
           "description": "Awesome and trippy",
           "recipe_type": "starter"
        }

    def test_empty_name(self):
        self.baseRecipe["name"] = ""
        with self.assertRaises(AssertionError):
            Recipe(**(self.baseRecipe))

    def test_cooking_lvl_nan(self):
        self.baseRecipe["cooking_lvl"] = "nan"
        with self.assertRaises(AssertionError):
            Recipe(**(self.baseRecipe))

    def test_cooking_lvl_oor(self):
        self.baseRecipe["cooking_lvl"] = 9999
        with self.assertRaises(AssertionError):
            Recipe(**(self.baseRecipe))

    def test_cooking_time_nan(self):
        self.baseRecipe["cooking_time"] = "nan"
        with self.assertRaises(AssertionError):
            Recipe(**(self.baseRecipe))

    def test_negative_cooking_time(self):
        self.baseRecipe["cooking_time"] = -42
        with self.assertRaises(AssertionError):
            Recipe(**(self.baseRecipe))

    def test_ingredients_notAList(self):
        self.baseRecipe["ingredients"] = {'bonjour': 'je suis pas une liste'}
        with self.assertRaises(AssertionError):
            Recipe(**(self.baseRecipe))

    def test_no_ingredients(self):
        self.baseRecipe["ingredients"] = list()
        with self.assertRaises(AssertionError):
            Recipe(**(self.baseRecipe))

    def test_wrong_recipe_type(self):
        self.baseRecipe["recipe_type"] = {'drink'}
        with self.assertRaises(AssertionError):
            Recipe(**(self.baseRecipe))

    def test_good_params(self):
        recipe = Recipe(**(self.baseRecipe))
        self.assertDictEqual(recipe.__dict__, self.baseRecipe)

    def test_to_str(self):
        br = self.baseRecipe
        recipe = Recipe(**(br))
        self.assertEqual(
            f"{br['name'].capitalize()}'s recipe:\n" +
            f"\tDescription:           {br['description']}\n" +
            f"\tType:                  {br['recipe_type']}\n" +
            f"\tDifficulty:            {br['cooking_lvl']}\n" +
            f"\tAverage cooking time:  {br['cooking_time']} minutes\n" +
            f"\tingredients:           {br['ingredients']}\n", str(recipe)
        )


class TestBookMethods(unittest.TestCase):
    def setUp(self):
        self.recipe_one = Recipe(
            name="recipe_one",
            cooking_lvl=2,
            cooking_time=20,
            ingredients=["sugar", "flour"],
            description="",
            recipe_type="starter"
        )
        self.cook_book = Book(
            name="Test cookBook"
        )

    def test_add_recipe(self):
        self.cook_book.add_recipe(self.recipe_one)
        self.assertDictEqual(
            self.cook_book.recipes_list[self.recipe_one.recipe_type],
            {self.recipe_one.name: self.recipe_one})
        with self.assertRaises(AlreadyExistsError):
            self.cook_book.add_recipe(self.recipe_one)
        with self.assertRaises(ValueError):
            self.cook_book.add_recipe("")

    def test_get_recipe_by_name(self):
        with self.assertRaises(NotFound):
            self.cook_book.get_recipe_by_name("jojo")
        self.cook_book.add_recipe(self.recipe_one)
        recipe = self.cook_book.get_recipe_by_name("recipe_one")
        self.assertEqual(recipe, self.recipe_one)

    def test_get_recipes_by_types(self):
        with self.assertRaises(NotFound):
            self.cook_book.get_recipes_by_types("drink")
        recipes = self.cook_book.get_recipes_by_types("starter")
        self.assertEqual(
            recipes,
            [],
        )
        self.cook_book.add_recipe(self.recipe_one)
        recipes = self.cook_book.get_recipes_by_types("starter")
        self.assertEqual(
            recipes,
            [self.recipe_one.name]
        )


if __name__ == "__main__":
    unittest.main()
