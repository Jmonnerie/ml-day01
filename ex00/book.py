# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    book.py                                            :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/26 12:31:26 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/26 12:33:56 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

from datetime import datetime
from recipe import Recipe


class NotFound(Exception):
    """ Raise an error

        :param typeOf: Word who describes what is not found.
        :param message: Message to send when the error raise
    """
    def __init__(self, message, *args):
        messageE = f"{args[0]}NotFound: {message}"
        super().__init__(messageE)


class AlreadyExistsError(Exception):
    def __init__(self, name):
        self.message = f"AlreadyExistsError: the receipe {name} already exists"


class Book():
    """
        A representation of a cookbook.

        :param name: The book's name.
        :type name: string.
        :raise AssertionErrror: When name is empty or not a string
    """

    def __init__(self, name: str):
        assert name, "name shouldn't be empty"
        assert isinstance(name, str), "name isn't a str"
        self.name = name
        self.last_update = datetime.now()
        self.creation_date = datetime.now()
        self.recipes_list = {"starter": {}, "lunch": {}, "dessert": {}}
        print(self)

    def __str__(self):
        recipies = []
        for value in self.recipes_list.values():
            recipies += [key for key in value.keys()]
        return f"{self.name.capitalize()} book:\n" +\
               f"\tRecipes:     {recipies}\n" +\
               f"\tCreation:    {self.creation_date}\n" +\
               f"\tLast Update: {self.last_update}\n"

    def get_recipe_by_name(self, name):
        """Print a recipe with the name `name` and return the instance"""
        for value in self.recipes_list.values():
            if name in value.keys():
                print(value[name])
                return(value[name])
        raise NotFound(f"the receipe {name} doesn't exist", "Recipe")

    def get_recipes_by_types(self, recipe_type):
        """Get all recipe names for a given recipe_type """
        if recipe_type in self.recipes_list:
            return list(self.recipes_list[recipe_type].keys())
        raise NotFound("the type {recipe_type} doesn't exist", "RecipeType")

    def add_recipe(self, recipe):
        """Add a recipe to the book and update last_update"""
        if not isinstance(recipe, Recipe):
            raise ValueError("the parameter isn't a Recipe object")
        if recipe.name in self.recipes_list[recipe.recipe_type].keys():
            raise AlreadyExistsError(recipe)
        self.recipes_list[recipe.recipe_type][recipe.name] = recipe
