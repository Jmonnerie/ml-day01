# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    game.py                                            :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/27 00:20:08 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/27 00:31:01 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#


class GotCharacter():
    def __init__(self, first_name=None, is_alive=True):
        self.first_name = first_name
        self.is_alive = is_alive


class Lannister(GotCharacter):
    """Class representing the famous lannister house.
       A lannister always pay his depts
    """
    def __init__(self, first_name=None, is_alive=True):
        super().__init__(first_name=first_name, is_alive=is_alive)
        self.family_name = "Lannister"
        self.house_words = "Hear Me Roar!"

    def print_house_words(self):
        print(self.house_words)

    def die(self):
        self.is_alive = False
